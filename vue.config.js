module.exports = {
  chainWebpack: (config) => {
    config.module
      .rule("glsl")
      .test(/\.glsl$/)
      .use("raw")
      .loader("raw-loader")
      .end()
      .use("glslify")
      .loader("glslify-loader")
      .end();

    config.module
      .rule("less")
      .use("less-loader")
      .loader("less-loader")
      .options({ javascriptEnabled: true })
      .end();

    config.plugin("html").tap((args) => {
      args[0].title = "Boucham Amine";
      return args;
    });

    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule
      .use("svg-inline-loader")
      .loader("svg-inline-loader")
      .end();
  },
};
