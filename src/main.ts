import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import {
  Layout,
  Col,
  Row,
  List,
  Avatar,
  Button,
  Tag,
  Divider,
  Pagination,
  Timeline,
  Modal,
  Dropdown,
  Menu,
  Badge,
  Steps,
} from "ant-design-vue";
import {
  ReadOutlined,
  GithubOutlined,
  GitlabOutlined,
  LinkedinFilled,
  MessageOutlined,
  LikeOutlined,
  getTwoToneColor,
  setTwoToneColor,
  TagTwoTone,
  TwitterOutlined,
} from "@ant-design/icons-vue";

import "ant-design-vue/dist/antd.css";
import "animate.css";

const app = createApp(App);
app.use(store).use(router);

app.use(Layout);
app.use(Col);
app.use(Row);
app.use(List);
app.use(Avatar);
app.use(Button);
app.use(Tag);
app.use(Divider);
app.use(Pagination);
app.use(Timeline);
app.use(Modal);
app.use(Dropdown);
app.use(Menu);
app.use(Badge);
app.use(Steps);
// Icon
app.component("ReadOutlined", ReadOutlined);
app.component("GithubOutlined", GithubOutlined);
app.component("GitlabOutlined", GitlabOutlined);
app.component("LinkedinFilled", LinkedinFilled);
app.component("MessageOutlined", MessageOutlined);
app.component("LikeOutlined", LikeOutlined);
app.component("TagTwoTone", TagTwoTone);
app.component("TwitterCircleFilled", TwitterOutlined);

setTwoToneColor("#a7a2cd");
getTwoToneColor(); // #eb2f96

app.mount("#app");
