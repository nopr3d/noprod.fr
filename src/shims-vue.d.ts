declare module "*.vue" {
  import { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
  export default Vue;
}
declare interface Window {
  doorbellOptions: any;
  doorbell: any;
}
declare module "pdfjs-dist";
declare module "ml5";
