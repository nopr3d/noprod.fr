import CustomScene from "@/gl/Scene";
import { isMobile } from "@/services";

const scene = new CustomScene();
export const background = (background: string) => {
  scene.init(
    background,
    document.getElementById("canvas") as HTMLCanvasElement
  );
  scene.run();
};

export const clean = () => {
  scene.setEmpty();
};
