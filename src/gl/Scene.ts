import {
  BoxGeometry,
  Clock,
  Color,
  Mesh,
  PerspectiveCamera,
  PlaneBufferGeometry,
  Scene,
  ShaderMaterial,
  SpotLight,
  WebGLRenderer,
} from "three";

import { onResize } from "@/gl/Resize";

import GlitchStorm from "@/gl/GlitchStorm.glsl";
import InfinitLoader from "@/gl/InfinitLoader.glsl";
import LineDistortion from "@/gl/LineDistortion.glsl";
import SpaceBlue from "@/gl/SpaceBlue.glsl";
import FloatingBox from "@/gl/FloatingBox.glsl";
import MovingCloud from "@/gl/MovingCloud.glsl";

import Glitch from "@/gl/frag/Glitch.glsl";

import vertex from "@/gl/DefaultVertex.glsl";

import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer.js";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass.js";

export default class CustomScene {
  private glsl = {
    Glitch,
    GlitchStorm,
    InfinitLoader,
    LineDistortion,
    vertex,
    SpaceBlue,
    FloatingBox,
    MovingCloud,
  };

  private scene!: Scene;
  private camera!: PerspectiveCamera;
  private geometry!: PlaneBufferGeometry;
  private light!: SpotLight;
  private material!: ShaderMaterial;
  private mesh!: Mesh;
  private renderer!: WebGLRenderer;
  private clock!: Clock;

  private composer!: EffectComposer;

  private id!: number;

  public init(fragment: string, canvas: HTMLCanvasElement) {
    this.scene = new Scene();
    this.scene.background = new Color(0xe3e3e3);
    this.light = new SpotLight(0xffffff, 1);

    const fov = 45;
    const aspect = window.innerWidth / window.innerHeight;
    const near = 1;
    const far = 100;

    const _uniforms = {
      uTime: { value: 0.0 },
      uResolution: { value: { x: window.innerWidth, y: window.innerHeight } },
      uMouse: { value: { x: 0, y: 0 } },
      uColor: { value: new Color(0xffffff) },
    };

    this.camera = new PerspectiveCamera(fov, aspect, near, far);
    this.geometry = new PlaneBufferGeometry(30, 10);

    this.material = new ShaderMaterial({
      vertexShader: this.glsl.vertex,
      fragmentShader: this.glsl[fragment as never],
      uniforms: {
        uTime: { value: 0.0 },
        uResolution: { value: { x: window.innerWidth, y: window.innerHeight } },
        uMouse: { value: { x: 0, y: 0 } },
        uColor: { value: new Color(0xffffff) },
      },
    });
    this.mesh = new Mesh(this.geometry, this.material);
    this.renderer = new WebGLRenderer({
      canvas: canvas,
    });

    this.renderer.setClearColor(0xffffff, 1);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.composer = new EffectComposer(this.renderer);
    this.scene.add(this.camera);
    this.scene.add(this.mesh);
    this.scene.add(this.light);
    this.mesh.position.set(0, 0, 0);
    this.camera.position.set(0, 0, 10);
    this.light.position.set(0, 0, 10);

    this.light.lookAt(this.mesh.position);
    this.camera.lookAt(this.mesh.position);

    this.clock = new Clock();
    const renderPass = new RenderPass(this.scene, this.camera);
    this.composer.addPass(renderPass);
    this.addEvents();
  }

  public setEmpty() {
    cancelAnimationFrame(this.id); // Stop the animation
    this.scene = null;
    this.composer = null;
    this.camera = null;
    this.clock = null;
    this.geometry = null;
    this.material = null;
    this.light = null;
    this.mesh = null;
    this.renderer = null;
  }

  public run() {
    this.id = window.requestAnimationFrame(this.run.bind(this));
    this.material.uniforms.uTime.value = this.clock.getElapsedTime();
    this.composer.render();
  }

  private addEvents(): void {
    window.addEventListener(
      "resize",
      onResize.bind(this, this.camera, this.renderer, this.material),
      false
    );
  }
}
