import { PerspectiveCamera, Renderer, ShaderMaterial } from "three";

export const onResize = (
  camera: PerspectiveCamera,
  renderer: Renderer,
  material: ShaderMaterial
) => {
  material.uniforms.uResolution = {
    value: { x: window.innerWidth, y: window.innerHeight },
  };
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
};
