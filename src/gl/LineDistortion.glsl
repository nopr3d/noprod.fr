/*
 * Original shader from: http://glslsandbox.com/e#68528.0
 * Edited for threejs by Boucham Amine
 */
#ifdef GL_ES
precision mediump float;
#endif

uniform float uTime;
uniform vec2 uResolution;

void main( void ) 
{
	vec2 p = ( gl_FragCoord.xy / uResolution.xy ) * 2.0 - 1.0;
	
	vec3 c = vec3( 0.0 );
	
	float amplitude = 0.15; 
	float glowT = sin(uTime) * 0.5 + 0.5;
	float glowFactor = mix( 0.05, 0.2, glowT );
	c += vec3(0.02, 0.01, 0.03) * ( glowFactor * abs( 1.0 / sin(p.x + sin( p.y + uTime ) * amplitude-0.3 ) ));
	c += vec3(0.02, 0.01, 0.09) * ( glowFactor * abs( 1.0 / sin(p.x + sin( p.y + uTime+1.00 ) * amplitude+0.4 ) ));

	gl_FragColor = vec4( c, 1.0 );

}