
#ifdef GL_ES
precision highp float;
precision highp int;
#endif

uniform sampler2D uTexture;
uniform vec2      uResolution;          
uniform float     uTime;          



float rand(vec2 co){
    return fract(cos(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}


void main(){
   vec3 uv = vec3(0.0);
   vec2 uv2 = vec2(0.0);
   vec2 nuv = gl_FragCoord.xy / uResolution.xy;
   vec3 texColor = vec3(0.0);

   if (rand(vec2(uTime)) < 0.7){
    texColor = texture2D(uTexture, vertTexCoord.st).rgb;
}
 else{
   texColor = texture2D(uTexture, nuv * vec2(rand(vec2(uTime)), rand(vec2(uTime * 0.99)))).rgb;
}
       
    float r = rand(vec2(uTime * 0.001));
    float r2 = rand(vec2(uTime * 0.1));
    if (nuv.y > rand(vec2(r2)) && nuv.y < r2 + rand(vec2(0.05 * uTime))){
    if (r < rand(vec2(uTime * 0.01))){
       
   if ((texColor.b + texColor.g + texColor.b)/3.0 < r * rand(vec2(0.4, 0.5)) * 2.0){
       
        uv.r -= sin(nuv.x * r * 0.1 * uTime ) * r * 7000.;
        uv.g += sin(vertTexCoord.y * vertTexCoord.x/2 * 0.006 * uTime) * r * 10 *rand(vec2(uTime * 0.1)) ;
        uv.b -= sin(nuv.y * nuv.x * 0.5 * uTime) * sin(nuv.y * nuv.x * 0.1) * r *  20. ;
        uv2 += vec2(sin(nuv.x * r * 0.1 * uTime ) * r );
   
}
       
    }
}

  texColor = texture2D(uTexture, vertTexCoord.st + uv2).rgb;
  texColor += uv;
   gl_FragColor = vec4(texColor, 1.0);  
   
}