/*
 * Original shader from: https://www.shadertoy.com/view/td3czf
 * Edited for threejs by Boucham Amine
 */

#ifdef GL_ES
precision mediump float;
#endif

uniform float uTime;
uniform vec2 uResolution;

// --------[ Original ShaderToy begins here ]---------- //
#define M_2PI 6.28318530
#define POINT_COUNT 10
const float len = 0.35;

const float speed = -0.5;
const float scale = 1.3;
float glow = 1.5;
float fading = 0.28;
float radius = 0.08;

float dot2(vec2 v ) { return dot(v,v); }

// quadratic bezier with parameter along the curve
// 2d version of https://www.shadertoy.com/view/ldj3Wh
vec2 sdBezier(vec2 pos, vec2 A, vec2 B, vec2 C)
{    
    vec2 a = B - A;
    vec2 b = A - 2.0*B + C;
    vec2 c = a * 2.0;
    vec2 d = A - pos;

    float kk = 1.0 / dot(b,b);
    float kx = kk * dot(a,b);
    float ky = kk * (2.0*dot(a,a)+dot(d,b)) / 3.0;
    float kz = kk * dot(d,a);      

    vec2 res;

    float p = ky - kx*kx;
    float p3 = p*p*p;
    float q = kx*(2.0*kx*kx - 3.0*ky) + kz;
    float h = q*q + 4.0*p3;

    if(h >= 0.0) 
    { 
        h = sqrt(h);
        vec2 x = (vec2(h, -h) - q) / 2.0;
        vec2 uv = sign(x)*pow(abs(x), vec2(1.0/3.0));
        float t = clamp(uv.x+uv.y-kx, 0.0, 1.0);

        // 1 root
        res = vec2(dot2(d+(c+b*t)*t),t);
    }
    else
    {
        float z = sqrt(-p);
        float v = acos( q/(p*z*2.0) ) / 3.0;
        float m = cos(v);
        float n = sin(v)*1.732050808;
        vec3 t = clamp( vec3(m+m,-n-m,n-m)*z-kx, 0.0, 1.0);
        
        // 3 roots, but only need two
        float dis = dot2(d+(c+b*t.x)*t.x);
        res = vec2(dis,t.x);

        dis = dot2(d+(c+b*t.y)*t.y);
        if( dis<res.x ) res = vec2(dis,t.y );
    }
    
    res.x = sqrt(res.x);
    return res;
}

vec2 leminiscate(float t){
    float x = cos(t) / (1.0 + sin(t) * sin(t));
    float y = sin(t) * cos(t) / (1.0 + sin(t) * sin(t));
    return vec2(x, y);
}

// inspired by https://www.shadertoy.com/view/wdy3DD
float map(float t, vec2 pos){
    vec2 p1 = leminiscate(fract(speed * t) * M_2PI);
    vec2 p2 = leminiscate(len + fract(speed * t) * M_2PI);
    vec2 c_prev;
    vec2 c = (p1 + p2) / 2.0;
    float d = 1e9;
    
    for(int i = 1; i < POINT_COUNT-1; i++){
        p1 = leminiscate(float(i) * len + fract(speed * t) * M_2PI);
        p2 = leminiscate(float(i+1) * len + fract(speed * t) * M_2PI);
        c_prev = c;
        c = (p1 + p2) / 2.0;
        vec2 f = sdBezier(pos, scale * c_prev, scale * p1, scale * c);
        d = min(d, f.x + fading * (f.y + float(i)) / float(POINT_COUNT));
    }
    return d;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ){
    vec2 uv = (2. * fragCoord - uResolution.xy) / uResolution.y;
	
    float dist = map(uTime, uv);
    
    vec3 col = vec3(0.2, 0.5, 1.0) * pow(radius/dist, glow);
    
    fragColor = vec4(col, 1.0);
}
// --------[ Original ShaderToy ends here ]---------- //

void main(void)
{
    mainImage(gl_FragColor, gl_FragCoord.xy);
}