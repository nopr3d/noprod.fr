import { createStore } from "vuex";

export default createStore({
  state: { showGl: false },
  mutations: {
    showGl(state) {
      state.showGl = true;
    },
    hideGl(state) {
      state.showGl = false;
    },
  },
  actions: {},
  modules: {},
});
