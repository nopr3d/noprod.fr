import { Application } from "@/models";

import Blog from "@/components/Blog.vue";
import Background from "@/components/Background.vue";
import About from "@/components/About.vue";
import PdfReader from "@/components/PdfReader.vue";
import FaceMask from "@/components/FaceMask.vue";
import TicTacToe from "@/components/TicTacToe.vue";

const Applications: Application[] = [
  {
    name: "About",
    component: About,
    icon: "info",
    startPosition: "",
    isOpen: true,
    isActive: true,
    fn: "",
    isFocus: false,
  },
  {
    name: "Blog",
    component: Blog,
    icon: "folder",
    startPosition: "",
    isOpen: false,
    isActive: true,
    fn: "",
    isFocus: false,
  },
  {
    name: "Background",
    component: Background,
    icon: "application",
    startPosition: "",
    isOpen: false,
    isActive: true,
    fn: "",
    isFocus: false,
  },
  {
    name: "CV",
    component: PdfReader,
    params: { pdf: "boucham_amine" },
    icon: "pdf",
    startPosition: "",
    isOpen: false,
    isActive: true,
    isFocus: false,
  },
  {
    name: "Face Mask Detector",
    component: FaceMask,
    icon: "application",
    startPosition: "",
    isOpen: false,
    isActive: true,
    isFocus: false,
  },
  {
    name: "Tic Tac Toe vs Bot",
    component: TicTacToe,
    icon: "tic_tac_toe",
    startPosition: "",
    isOpen: false,
    isActive: true,
    badge: "New !",
    isFocus: false,
  },
];

export { Applications };
