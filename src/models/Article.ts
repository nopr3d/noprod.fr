export interface Article {
  title: string;
  description?: string;
  tags: string[];
  link: string;
  pubDate: string | Date;
  isFrom: "devTo" | "Medium";
  picture?: string;
  reactions?: number;
  comments?: number;
  index?: number;
}
