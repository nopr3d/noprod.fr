export interface IconLink {
  link?: string;
  icon?: string;
  name?: string;
}
