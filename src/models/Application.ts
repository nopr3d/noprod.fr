export interface Application {
  name?: string;
  component?: any;
  icon?: string;
  fn?: any;
  startPosition?: string;
  isActive?: boolean;
  isFocus?: boolean;
  isOpen?: boolean;
  modalStyle?: string;
  contents?: any[];
  badge?: string;
  params?: { [key: string]: string };
}
