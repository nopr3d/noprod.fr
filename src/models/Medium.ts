export interface MediumItem {
  url: string;
  title: string;
  link: string;
  author: string;
  description: string;
  image: string;
  pubDate: string;
  categories: string[];
  thumbnail?: string;
}

export interface MediumResponse {
  status: string;
  feed: any;
  items: MediumItem[];
}
