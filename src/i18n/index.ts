import { createI18n } from "vue-i18n";

const messages = {
  en: {
    actionBar: { leftButton: "Start" },
    presentation: "Presentation",
    contact: "Contact",
    articles: "Articles",
    xp: "Experience",
    dev: "Hi, I'm a fullstack developer, passionate about the world of IT",
    devWhy: "Solving problems is fun, so give me yours !",
    discussCoffee: "Wanna chat over coffee?",
    stuff: "Here you can find some articles I wrote",
  },
  fr: {
    presentation: "Présentation",
    contact: "Contact",
    articles: "Articles",
    xp: "Expérience",
    dev:
      "Je suis un développeur fullstack, passionné par le monde de l'informatique.",
    devWhy: "Résoudre des problèmes m'amuse donc confiez moi les votres !",
    discussCoffee: "Envie de discuter autour d'un café ?",
    stuff: "Vous trouverez ici quelques articles | tuto que j'ai redigé",
  },
};

// 2. Create i18n instance with options
export const i18n = createI18n({
  locale: "en", // set locale
  fallbackLocale: "en", // set fallback locale
  messages, // set locale messages
  // If you need to specify other options, you can set other options
  // ...
});

export const setLocal = (locale: string) => {
  i18n.global.locale = locale;
};
