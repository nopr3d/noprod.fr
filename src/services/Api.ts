import { DevToResponse, MediumItem, MediumResponse, Article } from "@/models";
import Axios, { AxiosResponse } from "axios";

export const getDevToData = Axios.request<
  string,
  AxiosResponse<DevToResponse[]>
>({
  url: `${process.env.VUE_APP_DEV_TO}&timestamp=${new Date().getTime()}}`,
  method: "GET",
});

export const getMediumData = Axios.request<
  string,
  AxiosResponse<MediumResponse>
>({
  url: process.env.VUE_APP_MEDIUM,
  method: "GET",
});

const mediumToArticle = (mediumData: MediumItem[]): Article[] => {
  return mediumData.map((data: MediumItem) => {
    return {
      title: data.title,
      isFrom: "Medium",
      link: data.link,
      pubDate: data.pubDate,
      tags: data.categories,
      picture: data.thumbnail,
    };
  });
};
const devToActicle = (devToData: DevToResponse[]): Article[] => {
  return devToData.map((data: DevToResponse) => {
    return {
      title: data.title,
      isFrom: "devTo",
      link: data.canonical_url,
      pubDate: data.published_at,
      tags: data.tag_list,
      picture: data.social_image,
      reactions: data.positive_reactions_count,
      comments: data.comments_count,
      description: data.description,
    };
  });
};

/**
 * Map devTo and Medium model to Article model
 */
const mapResponse = (
  holder: Article[],
  req: AxiosResponse<DevToResponse[] | MediumResponse>
) => {
  Array.isArray(req.data)
    ? (holder = [...holder, ...devToActicle(req.data)])
    : (holder = [...holder, ...mediumToArticle(req.data.items)]);
  return holder;
};

export const getAllArticles = () => {
  var index = 0;
  return Promise.all([getDevToData, getMediumData]).then((requests) => {
    let _temp: Article[] = [];
    requests.forEach(
      async (req: AxiosResponse<DevToResponse[] | MediumResponse>) => {
        _temp = mapResponse(_temp, req);
      }
    );
    _temp.forEach((resp) => {
      resp.index = index;
      index++;
    });
    return _temp;
  });
};
