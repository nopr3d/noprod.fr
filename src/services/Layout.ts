import { Application } from "@/models";

import interact from "interactjs";
function dragMoveListener(event: any) {
  var target = event.target;
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute("data-x")) || 0) + event.dx;
  var y = (parseFloat(target.getAttribute("data-y")) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform = target.style.transform =
    "translate(" + x + "px, " + y + "px)";

  // update the posiion attributes
  target.setAttribute("data-x", x);
  target.setAttribute("data-y", y);
}

const initLayout = (apps: Application[]) => {
  interact(".draggable").draggable({
    inertia: true,
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: "parent",
        endOnly: true,
      }),
    ],

    listeners: {
      move: dragMoveListener,
    },
  });

  interact(".draggable-modal").draggable({
    allowFrom: ".ant-modal-title",
    inertia: true,
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: "body",
      }),
    ],

    listeners: {
      move: dragMoveListener,
    },
  });

  interact(".draggable-app").draggable({
    inertia: true,
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: "body",
      }),
    ],

    listeners: {
      move: dragMoveListener,
    },
  });
};

const displayModal = (app: Application) => {
  //@ts-ignore
  const modal = document.getElementsByClassName(
    //@ts-ignore
    `__modal_${app.name.toLowerCase().replaceAll(" ", "_")}`
  )[0];
  //@ts-ignore
  setTimeout(() => {
    modal.classList.remove("opacity-0");
  }, 350);
};

const hideModal = (app: Application) => {
  const modal = document.getElementsByClassName(
    //@ts-ignore
    `__modal_${app.name.toLowerCase().replaceAll(" ", "_")}`
  )[0];
  //@ts-ignore
  modal.classList.add("opacity-0");
};

const firstPlan = (app: Application) => {
  const modal: any =
    document.getElementsByClassName(
      //@ts-ignore
      `__modal_${app.name.toLowerCase().replaceAll(" ", "_")}`
    )[0] || [];
  //@ts-ignore
  if (modal.classList) modal.classList.add("first-plan");
};

const secondPlan = (app: Application) => {
  const modal: any =
    document.getElementsByClassName(
      //@ts-ignore
      `__modal_${app.name.toLowerCase().replaceAll(" ", "_")}`
    )[0] || [];
  //@ts-ignore
  if (modal.classList) modal.classList.remove("first-plan");
};

export { initLayout, displayModal, hideModal, secondPlan, firstPlan };
