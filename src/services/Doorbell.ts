export const initBell = () => {
  window.doorbellOptions = {
    id: "12075",
    appKey: "LCMh7VdaOHpe7hfhaTVg2vWbHPRKj0ZzRXTcPQSucgXBe5qVO8cIT4kl92kJQGVP",
    onLoad: () => {
      let i = 0;
      const interval = setInterval(() => {
        if (window.doorbell.send || i < 10) {
          window.doorbell.send(
            `Ping at :${new Date().toLocaleString()}`,
            "ping@ping.com",
            "null",
            null
          );
          console.log("doorbell @ ping at :" + new Date().toLocaleString());
          clearInterval(interval);
        }
        console.log("doorbell @ try ping at :" + new Date().toLocaleString());
        i++;
      }, 1000);
      if (i > 10) {
        clearInterval(interval);
      }
    },
  };
  require("@/lib/doorbell");
};
