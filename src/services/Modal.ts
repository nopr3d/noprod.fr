import { BehaviorSubject } from "rxjs";
export class Modal {
  private modals = new BehaviorSubject([]);
  private $modals = this.modals.asObservable();

  public register(dataObj: any) {
    const currentValue = this.modals.value;
    const updatedValue = [...currentValue, dataObj];

    this.modals.next(updatedValue);
  }

  public remove(dataObj: any) {
    const holder = [...this.modals.value];

    holder.forEach((item, index) => {
      if (item === dataObj) {
        holder.slice(index, 1);
      }
    });

    this.modals.next(holder);
  }

  public getAll() {
    return this.$modals;
  }

  public reset() {
    this.modals.next([]);
  }
}
